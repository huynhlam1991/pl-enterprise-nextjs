import Image from 'next/image'
import Message from './message'
import btnLogin from './login/btnLogin'
import BtnLogin from './login/btnLogin'
export default function Home() {
  return (
    <>
  <div className="background">
    <div className="shape" />
    <div className="shape" />
  </div>
  <form>
    <h3>Login Here</h3>
    <label htmlFor="username">Username</label>
    <input type="text" placeholder="Email or Phone" id="username" />
    <label htmlFor="password">Password</label>
    <input type="password" placeholder="Password" id="password" />
    <>
    <BtnLogin></BtnLogin>
    </>
    <div className="social">
      
      <div className="fb">
        <i className="fab" /> Login With SSO
      </div>
    </div>
  </form>
</>

  )
}

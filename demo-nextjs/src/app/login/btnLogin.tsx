'use client';
import React from 'react';
var count = 0;
const BtnLogin = () => {
  // Step 2: Define the click event handler function
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    // Your logic or actions to be performed when the button is clicked
    event.preventDefault();
    console.log('Button clicked!');
    alert('This function not handle. Please login with SSO')
  };

  return (
    <div>
      {/* Step 3: Attach the click event handler to the button */}
      <button onClick={handleClick}>Log In</button>
    </div>
  );
};

export default BtnLogin;